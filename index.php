<?php
include_once ('vendor/autoload.php');

$fileStore = new \League\Flysystem\Adapter\Local(__DIR__.'/files/');
$filesystem = new \League\Flysystem\Filesystem($fileStore);

$contents = $filesystem->listContents('');

foreach ($contents as $object) {
    echo $object['basename'].' is located at '.$object['path'].' and is a '.$object['type'];
}
